format:
	cargo fmt

mv: 
	cp target/

# TODO: merge two build commands in one
build-debug: format
	cargo build
	cp target/debug/libstereo2mono.so stereo2mono.lv2/

build: format
	cargo build --release
	cp target/release/libstereo2mono.so stereo2mono.lv2/
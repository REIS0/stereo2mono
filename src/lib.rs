use itertools::izip;
use lv2::prelude::*;

#[derive(PortCollection)]
struct Ports {
    input_right: InputPort<Audio>,
    input_left: InputPort<Audio>,
    output_right: OutputPort<Audio>,
    output_left: OutputPort<Audio>,
}

#[uri("https://gitlab.com/REIS0/stereo2mono")]
struct Stereo2Mono;

impl Plugin for Stereo2Mono {
    type Ports = Ports;

    type InitFeatures = ();
    type AudioFeatures = ();

    fn new(_plugin_info: &PluginInfo, _features: &mut ()) -> Option<Self> {
        Some(Self)
    }

    fn run(&mut self, ports: &mut Ports, _features: &mut (), _: u32) {
        for (in_frame_right, in_frame_left, out_frame_right, out_frame_left) in izip!(
            ports.input_right.iter(),
            ports.input_left.iter(),
            ports.output_right.iter_mut(),
            ports.output_left.iter_mut()
        ) {
            *out_frame_right = in_frame_left * 0.5 + in_frame_right * 0.5;
            *out_frame_left = in_frame_left * 0.5 + in_frame_right * 0.5;
        }
    }
}

lv2_descriptors!(Stereo2Mono);
